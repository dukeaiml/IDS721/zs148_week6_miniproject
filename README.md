# Week 6 Mini-Project

## Author: 
Ziyu Shi

## Requirement
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

## Lambda functionality
In this project, I have completed an AWS lambda function that calculates the frequency of every character in the input json file and returns the result in lexicographical order.

## Instruction Steps
1. **Add logging to the Rust lambda function**

    Make sure to add following dependencies in the Cargo.toml
    ```
    tracing = "0.1.37"
    tracing-subscriber = "0.2.0"
    ```
    Then initialize logging in the main function and print a message at the beginning of the service
    ```
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("Unable to global default");

    info!("Starting the redactr service...");
    ```
2. **Enable the AWS X-Ray tracing**

    Create a new IAM role for this project. Ensuring that this new role has the following permissions: `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess`, `AWSXRayDaemonWriteAccess`.
    Then build and deploy the lambda function onto the AWS.
    Go to the AWS Lambda, and enable the X-Ray active tracing functionality in the `Configuration -> Monitoring and operations tools`.

3. **Test the logging and AWS X-Ray tracing**
    
    To test with `cargo`, first run the Lambda emulator built in with the watch subcommand:
    ```
    cargo lambda watch
    ```
    Then test with:
    ```
    cargo lambda invoke --data-ascii "{ \"data\": \"Hello World!\" }"
    ```
    To test with HTTP method, use the command to test the API gateway:
    ```
    curl -X POST https://qx4bf0p7ki.execute-api.us-east-2.amazonaws.com/lambda_function_stage/ \
        -H 'content-type: application/json' \
        -d '{ "data": "Hello World!"}'
    ```
    After these, you can go to AWS to check the logs and traces in the Monitor section of the AWS Lambda.

## Screenshots:

**Add logging to Rust lambda function**
![image](/lambda_function/photos/addLogToCode.png)

**Display the logging function**
![image](/lambda_function/photos/displayLog.png)

**Enable the AWS X-Ray tracing**
![image](/lambda_function/photos/xRayEnable.png)

**Logs in CloudWatch and AWS X-Ray traces**
![image](/lambda_function/photos/XRayTracing.png)

**Traces details**
![image](/lambda_function/photos/traceDetail.png)